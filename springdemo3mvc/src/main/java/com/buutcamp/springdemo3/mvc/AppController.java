package com.buutcamp.springdemo3.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@Controller
@EnableWebMvc
public class AppController {

    @RequestMapping(value="/",method= RequestMethod.GET)
    public String frontPageGET(Model model) {

        return "front-page";
    }

}
